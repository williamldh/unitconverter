#!/usr/bin/env python
# coding: utf-8

from dash import callback, html, dcc, callback_context
from dash.dependencies import Input, Output
from scipy.constants import convert_temperature

def block_temperature():
    return html.Div(children=[
        'Celsius (°C)', html.Br(), dcc.Input(id="celsius", value=None,type="number"),html.Br(),
        'Kelvin (K)', html.Br(), dcc.Input(id="kelvin", value=None, type="number"),html.Br(),
        'Fahrenheit (°F)', html.Br(), dcc.Input(id="fahrenheit", value=None, type="number"),html.Br(),
        'Rankine (°R)', html.Br(), dcc.Input(id="rankine", value=None, type="number"),
        ])

@callback(
    Output("celsius", "value"),
    Output("fahrenheit", "value"),
    Output("kelvin", "value"),
    Output("rankine", "value"),
    Input("celsius", "value"),
    Input("fahrenheit", "value"),
    Input("kelvin", "value"),
    Input("rankine", "value"),
)
def temperature_converter(celsius, fahrenheit, kelvin, rankine):
    ctx = callback_context
    unit = ctx.triggered[0]["prop_id"].split(".")[0]
    if unit == "celsius":
        fahrenheit = None if celsius is None else convert_temperature(float(celsius), 'c', 'f')
        kelvin = None if celsius is None else convert_temperature(float(celsius), 'c', 'k')
        rankine = None if celsius is None else convert_temperature(float(celsius), 'c', 'r')
    elif unit == "fahrenheit":
        celsius = None if fahrenheit is None else convert_temperature(float(fahrenheit), 'f', 'c')
        kelvin = None if fahrenheit is None else convert_temperature(float(fahrenheit), 'f', 'k')
        rankine = None if fahrenheit is None else convert_temperature(float(fahrenheit), 'f', 'r')
    elif unit == "kelvin":
        celsius = None if kelvin is None else convert_temperature(float(kelvin), 'k', 'c')
        fahrenheit = None if kelvin is None else convert_temperature(float(kelvin), 'k', 'f')
        rankine = None if kelvin is None else convert_temperature(float(kelvin), 'k', 'r')
    elif unit == "rankine":
        celsius = None if rankine is None else convert_temperature(float(rankine), 'r', 'c')
        fahrenheit = None if rankine is None else convert_temperature(float(rankine), 'r', 'f')
        kelvin = None if rankine is None else convert_temperature(float(rankine), 'r', 'k')
    return celsius, fahrenheit, kelvin, rankine

